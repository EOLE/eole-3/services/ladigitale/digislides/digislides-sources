<?php

session_start();

require 'headers.php';

if (!empty($_POST['presentation'])) {
	$presentation = $_POST['presentation'];
	unset($_SESSION['digislides'][$presentation]['reponse']);
	echo 'session_terminee';
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
