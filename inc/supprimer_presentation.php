<?php

session_start();

require 'headers.php';

if (!empty($_POST['presentation'])) {
	require 'db.php';
	$reponse = '';
	$presentation = $_POST['presentation'];
	if (isset($_SESSION['digislides'][$presentation]['reponse'])) {
		$reponse = $_SESSION['digislides'][$presentation]['reponse'];
	}
	$stmt = $db->prepare('SELECT reponse FROM digislides_presentations WHERE url = :url');
	if ($stmt->execute(array('url' => $presentation))) {
		$resultat = $stmt->fetchAll();
		if (!$resultat) {
			echo 'contenu_inexistant';
		} else if ($resultat[0]['reponse'] === $reponse) {
			$stmt = $db->prepare('DELETE FROM digislides_presentations WHERE url = :url');
			if ($stmt->execute(array('url' => $presentation))) {
				if (file_exists('../fichiers/' . $presentation)) {
					supprimer('../fichiers/' . $presentation);
				}
				echo 'presentation_supprimee';
			} else {
				echo 'erreur';
			}
		} else {
			echo 'non_autorise';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

function supprimer ($path) {
	if (is_dir($path) === true) {
		$files = array_diff(scandir($path), array('.', '..'));
		foreach ($files as $file) {
			supprimer(realpath($path) . '/' . $file);
		}
		return rmdir($path);
	} else if (is_file($path) === true) {
		return unlink($path);
	}
	return false;
}

?>
