<?php

session_start();

require 'headers.php';

$_POST = json_decode(file_get_contents('php://input'), true);

if (!empty($_POST['presentation']) && !empty($_POST['donnees'])) {
	require 'db.php';
	$reponse = '';
	$presentation = $_POST['presentation'];
	if (isset($_SESSION['digislides'][$presentation]['reponse'])) {
		$reponse = $_SESSION['digislides'][$presentation]['reponse'];
	}
	$stmt = $db->prepare('SELECT reponse FROM digislides_presentations WHERE url = :url');
	if ($stmt->execute(array('url' => $presentation))) {
		$resultat = $stmt->fetchAll();
		if (!$resultat) {
			echo 'contenu_inexistant';
		} else if ($resultat[0]['reponse'] === $reponse) {
			$donnees = $_POST['donnees'];
			$stmt = $db->prepare('UPDATE digislides_presentations SET donnees = :donnees WHERE url = :url');
			if ($stmt->execute(array('donnees' => json_encode($donnees), 'url' => $presentation))) {
				if (!empty($_POST['image'])) {
					$image = $_POST['image'];
					if (file_exists('../fichiers/' . $presentation . '/' . $image)) {
						unlink('../fichiers/' . $presentation . '/' . $image);
					}
					if (file_exists('../fichiers/' . $presentation . '/vignette_' . $image)) {
						unlink('../fichiers/' . $presentation . '/vignette_' . $image);
					}
				}
				if (!empty($_POST['audio'])) {
					$audio = $_POST['audio'];
					if (file_exists('../fichiers/' . $presentation . '/' . $audio)) {
						unlink('../fichiers/' . $presentation . '/' . $audio);
					}
				}
				if (!empty($_POST['fichiersASupprimer'])) {
					$fichiers = json_decode($_POST['fichiersASupprimer'], true);
					foreach ($fichiers as $fichier) {
						if (file_exists('../fichiers/' . $presentation . '/' . $fichier)) {
							unlink('../fichiers/' . $presentation . '/' . $fichier);
						}
					}
				}
				if (!empty($_POST['fichiersADupliquer'])) {
					$fichiers = json_decode($_POST['fichiersADupliquer'], true);
					foreach ($fichiers as $fichier) {
						if (file_exists('../fichiers/' . $presentation . '/' . $fichier['ancien'])) {
							copy('../fichiers/' . $presentation . '/' . $fichier['ancien'], '../fichiers/' . $presentation . '/' . $fichier['nouveau']);
						}
					}
				}
				echo 'presentation_modifiee';
			} else {
				echo 'erreur';
			}
		} else {
			echo 'non_autorise';
		}
	} else {
		echo 'erreur';
	}
	$db = null;
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
