<?php

session_start();

require 'headers.php';

if (!empty($_POST['presentation'])) {
	$presentation = $_POST['presentation'];
	if (file_exists('../fichiers/' . $presentation)) {
		$fichiers = glob('../fichiers/' . $presentation . '/' . '*.*');
		foreach ($fichiers as $f) {
			unlink($f);
		}
	}
	echo 'dossier_vide';
	exit();
} else {
	header('Location: ../');
	exit();
}

?>
