# Digislides

Digislides est une application simple pour créer des présentations. 

Elle est publiée sous licence GNU AGPLv3.
Sauf les fontes Roboto Slab et Material Icons (Apache License Version 2.0), les fontes HKGrotesk, Montserrat, Quicksand, Lato, Open Sans, Source Sans Pro, League Gothic et News Cycle (Sil Open Font Licence 1.1) et la fonte Ubuntu (Ubuntu Font Licence 1.0)

### Préparation et installation des dépendances
```
npm install
```

### Lancement du serveur de développement
```
npm run dev
```

### Variables d'environnement (fichier .env.production à créer à la racine avant compilation)
```
AUTHORIZED_DOMAINS (* ou liste des domaines autorisés pour les requêtes POST et l'API, séparés par une virgule)
VITE_DOMAIN (hôte de l'application, par exemple https://ladigitale.dev)
VITE_FOLDER (dossier de l'application, par exemple /digislides/)
VITE_PIXABAY_API_KEY (clé API Pixabay)
```

### Compilation et minification des fichiers
```
npm run build
```

### Serveur PHP nécessaire pour l'API
```
php -S 127.0.0.1:8000 (pour le développement uniquement)
```

### Configuration .htaccess pour serveur Apache (production)
```
RewriteEngine on
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)$ index.html
```

### Démo
https://ladigitale.dev/digislides/

### Soutien
Open Collective : https://opencollective.com/ladigitale

Liberapay : https://liberapay.com/ladigitale/

### Remerciements et crédits
Traduction en italien par Paolo Mauri (https://codeberg.org/maupao)
